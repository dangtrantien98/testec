const Header = () => {
  return (
    <header>
      <nav className="py-3 fixed-top bg-white">
        <div className="container px-4 pb-2 d-flex justify-content-between">
          <a className="m-0" href="#">
            <img
              width={236}
              height={61}
              src="./assets/Testec-logo.png"
              alt="logo"
            />
          </a>
          <div className="menu_header d-flex">
            <ul>
              <li>
                <a className="active" href="#">
                  Home
                </a>
              </li>
              <li>
                <a href="#">Services</a>
              </li>
              <li>
                <a href="#">Works</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
              <li>
                <a href="#">Careers</a>
              </li>
              <li>
                <a className="btn btn_main" href="#">
                  Book Now
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Header;
