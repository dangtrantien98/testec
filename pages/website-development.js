import Layout from "../components/Layout";

const WebsiteDevelopment = () => {
  return (
    <Layout page="website-development">
      <section>
        <div className="title-container">
          <div className="title">
            <div className="breadcrums mt-2">
              <a href="#">Home</a>
              &gt;
              <a href="#">Services</a>
              &gt; Website Development
            </div>
            <p>Design</p>
            <h2>
              Website
              <br />
              Development
            </h2>
            <a className="btn btn_main btn_round" href="#">
              Book Now
            </a>
          </div>
          <img
            className="title-img"
            src="./assets/website-development/Website-Development-1.png"
            alt="title-img"
          />
        </div>
        <div className="d-flex align-items-start ms-3 me-4 mt-5 px-5">
          <img src="./assets/Dots.png" alt="dots" />
          <div className="d-flex align-items-center">
            <div className="d-flex flex-column align-items-center">
              <img
                src="./assets/website-development/Website-Development-2.gif"
                alt="img-1"
              />
              <div className="my-5 d-flex flex-column">
                <span>
                  When it comes to creating notable visibility and traffic
                  online, our experienced website developers leave nothing to
                  chance. They perform extensive research to sift through the
                  algorithms of some of the most popular brand sites and this
                  information is applied to help bolster your online presence.
                </span>
                <span>
                  The end result is a sophisticated and clutter-free website
                  that is optimised for smooth performance across all platforms
                  and devices.
                </span>
              </div>
              <img
                src="./assets/website-development/Website-Development-4.gif"
                alt="img-2"
              />
            </div>
            <div className="ps-5 me-5 d-flex flex-column align-items-center">
              <div className="d-flex flex-column">
                <span>
                  The infrastructure we have at TESTEC helps us provide our
                  clients with unique, high quality and flexible web solutions.
                  With a creative and professional team, it allows us to
                  reliably ensure a timely delivery with all the features and
                  ideal functionalities in place.
                </span>
                <span>
                  With this, the client’s users will have easy access to
                  everything they need as soon as they visit the website. Making
                  their experiences seamless and convenient.
                </span>
              </div>
              <img
                className="my-5"
                src="./assets/website-development/Website-Development-3.gif"
                alt="img-3"
              />
              <div className="d-flex flex-column">
                <span>
                  Our team works tirelessly to deliver you with all the best web
                  development solutions possible, for the growth of your
                  business. With quality in mind, we ensure your preferences are
                  an integral part of each and every decision we make throughout
                  the development process, making the final product a flawless
                  one.
                </span>
                <span>
                  Our websites have consistenly been proven as the perfect
                  stepping stones for brands to rapidly grow their businesses
                  while giving them a competitive edge in their respective
                  markets.
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="stay_connect d-flex justify-content-between">
          <div className="ps-2">
            <h2>
              Stay Connected
              <br />
              Get to know us, Say hello!
            </h2>
            <ul>
              <li>Mobile Development</li>
              <li>Website Development</li>
              <li>Enterprise Solutions</li>
              <li>Cloud Solutions</li>
              <li>UI/UX Design</li>
              <li>QA Testing</li>
              <li>Digital Marketing</li>
              <li>IT Consulting</li>
            </ul>
            <a className="btn btn_main btn_round" href="#">
              Contact Us
            </a>
          </div>
          <div>
            <img
              width={600}
              height={500}
              src="./assets/Stay-Connected.png"
              alt="stay-connect"
            />
            <img className="pt-5" src="./assets/Dots.png" alt="dots" />
          </div>
        </div>
        <div className="contact">
          <h2>
            Ready to turn
            <br />
            your ideas into reality?
          </h2>
          <p>
            Contact us now to know more on how you can leverage your business!
          </p>
          <a className="btn btn_main btn_round" href="#">
            Contact Us
            <svg
              className="ms-2"
              xmlns="http://www.w3.org/2000/svg"
              width={18}
              height={18}
              viewBox="0 0 24 25"
              fill="none"
            >
              <path
                d="M5.49002 0.47998V2.91198H19.937L0.900024 23.0854L2.518 24.8L21.555 4.62654V19.936H23.85V0.47998H5.49002Z"
                fill="white"
              />
            </svg>
          </a>
        </div>
      </section>
    </Layout>
  );
};

export default WebsiteDevelopment;
