import Layout from "../components/Layout";

const EnterpriseSolutions = () => {
  return (
    <Layout page="enterprise-solutions">
      <section>
        <div className="title-container">
          <div className="title">
            <div className="breadcrums mt-2">
              <a href="#">Home</a>
              &gt;
              <a href="#">Services</a>
              &gt; Enterprise Solutions
            </div>
            <p>Solutions</p>
            <h2>
              Enterprise
              <br />
              Solutions
            </h2>
            <a className="btn btn_main btn_round" href="#">
              Book Now
            </a>
          </div>
          <img
            className="title-img pe-5"
            src="./assets/enterprise-solutions/Enterprise-Solutions-1.png"
            alt="title-img"
          />
        </div>
        <div className="section-1">
          <img className="mt-5" src="./assets/Dots.png" alt="dots" />
          <img
            width={620}
            height={620}
            src="./assets/enterprise-solutions/Enterprise-Solutions-2.gif"
            alt="section-1"
          />
          <span>
            Digital communication is taking on new importance for business. Our
            team makes sure that you are always ready with our enterprise
            mobility solutions to take up new challenges and adapt quickly.
          </span>
        </div>
        <div className="section-2 container ps-4">
          <h2>Grow Beyond</h2>
          <div className="container mt-5 ms-4 d-flex">
            <div>
              <img
                className="pt-5 me-5 pe-3"
                src="./assets/enterprise-solutions/Image-1.png"
                alt="image-1"
              />
            </div>
            <div className="image-2 pt-5">
              <img
                width={400}
                height={450}
                src="./assets/enterprise-solutions/Image-2.png"
                alt="image-2"
              />
            </div>
            <div>
              <img
                className="ms-5 pt-5"
                src="./assets/enterprise-solutions/Image-3.png"
                alt="image-3"
              />
            </div>
          </div>
        </div>
        <div className="section-3">
          <div className="d-flex">
            <div>
              <img
                src="./assets/enterprise-solutions/Group-Img-1.png"
                alt="group-img-1"
              />
              <img
                src="./assets/enterprise-solutions/Group-Img-2.png"
                alt="group-img-2"
              />
            </div>
            <div className="pt-5 mt-4">
              <img
                src="./assets/enterprise-solutions/Group-Img-3.png"
                alt="group-img-3"
              />
              <img
                src="./assets/enterprise-solutions/Group-Img-4.png"
                alt="group-img-4"
              />
            </div>
          </div>
          <div className="discribe">
            <h2>Start With Us</h2>
            <span>
              Digital communication is taking on new importance for business.
              Our team makes sure that you are always ready with our enterprise
              mobility solutions to take up new challenges and adapt quickly.
            </span>
          </div>
        </div>
        <div className="stay_connect d-flex justify-content-between">
          <div className="ps-2">
            <h2>
              Stay Connected
              <br />
              Get to know us, Say hello!
            </h2>
            <ul>
              <li>Mobile Development</li>
              <li>Website Development</li>
              <li>Enterprise Solutions</li>
              <li>Cloud Solutions</li>
              <li>UI/UX Design</li>
              <li>QA Testing</li>
              <li>Digital Marketing</li>
              <li>IT Consulting</li>
            </ul>
            <a className="btn btn_main btn_round" href="#">
              Contact Us
            </a>
          </div>
          <div>
            <img
              width={600}
              height={500}
              src="./assets/Stay-Connected.png"
              alt="stay-connect"
            />
            <img className="pt-5" src="./assets/Dots.png" alt="dots" />
          </div>
        </div>
        <div className="contact">
          <h2>
            Ready to turn
            <br />
            your ideas into reality?
          </h2>
          <p>
            Contact us now to know more on how you can leverage your business!
          </p>
          <a className="btn btn_main btn_round" href="#">
            Contact Us
            <svg
              className="ms-2"
              xmlns="http://www.w3.org/2000/svg"
              width={18}
              height={18}
              viewBox="0 0 24 25"
              fill="none"
            >
              <path
                d="M5.49002 0.47998V2.91198H19.937L0.900024 23.0854L2.518 24.8L21.555 4.62654V19.936H23.85V0.47998H5.49002Z"
                fill="white"
              />
            </svg>
          </a>
        </div>
      </section>
    </Layout>
  );
};

export default EnterpriseSolutions;
