import Layout from "../components/Layout";

const CloudSolutions = () => {
  return (
    <Layout page="cloud-solutions">
      <section>
        <div className="title-container">
          <div className="title">
            <div className="breadcrums mt-2">
              <a href="#">Home</a>
              &gt;
              <a href="#">Services</a>
              &gt; Cloud Solutions
            </div>
            <p>Solutions</p>
            <h2>
              Cloud
              <br />
              Solutions
            </h2>
            <a className="btn btn_main btn_round" href="#">
              Book Now
            </a>
          </div>
          <img
            className="title-img pe-5"
            src="./assets/cloud-solutions/Cloud-Solutions-1.png"
            alt="title-img"
          />
        </div>
        <div className="section-1">
          <img src="./assets/Dots.png" alt="dots" />
          <img
            src="./assets/cloud-solutions/Cloud-Solutions-2.gif"
            alt="section-1"
          />
          <span>
            Cloud solutions has always been proven to help businesses scale new
            heights. At TESTEC, we provide outstanding cloud business solutions
            to prepare your business for the digital world. Grow and scale your
            business with our Cloud Solutions!
          </span>
        </div>
        <div className="section-2 container ps-4">
          <h2>We provide Cloud Solutions tailored to your business needs</h2>
          <div className="card-container">
            <div className="card">
              <img
                width={100}
                height={100}
                src="./assets/cloud-solutions/Group-1.png"
                alt="group-1"
              />
              <span>
                To provide growth that helps businesses to improve efficiently,
                we have the latest technology resources that sets the
                benchmarks.
              </span>
            </div>
            <div className="card">
              <img
                width={100}
                height={100}
                src="./assets/cloud-solutions/Group-2.png"
                alt="group-2"
              />
              <span>
                With our cloud-technologies, businesses can be sure of top-notch
                performance and quality. Our sophisticated solutions enhance
                business productivity and profitability .
              </span>
            </div>
            <div className="card">
              <img
                width={100}
                height={100}
                src="./assets/cloud-solutions/Group-3.png"
                alt="group-3"
              />
              <span>
                Get the freedom to access all your services, databases and apps
                directly from the web with our cloud solutions. Save your
                hardware and additional server costs with our online storage
                space.
              </span>
            </div>
          </div>
        </div>
        <div className="stay_connect d-flex justify-content-between">
          <div className="ps-2">
            <h2>
              Stay Connected
              <br />
              Get to know us, Say hello!
            </h2>
            <ul>
              <li>Mobile Development</li>
              <li>Website Development</li>
              <li>Enterprise Solutions</li>
              <li>Cloud Solutions</li>
              <li>UI/UX Design</li>
              <li>QA Testing</li>
              <li>Digital Marketing</li>
              <li>IT Consulting</li>
            </ul>
            <a className="btn btn_main btn_round" href="#">
              Contact Us
            </a>
          </div>
          <div>
            <img
              width={600}
              height={500}
              src="./assets/Stay-Connected.png"
              alt="stay-connect"
            />
            <img className="pt-5" src="./assets/Dots.png" alt="dots" />
          </div>
        </div>
        <div className="contact">
          <h2>
            Ready to turn
            <br />
            your ideas into reality?
          </h2>
          <p>
            Contact us now to know more on how you can leverage your business!
          </p>
          <a className="btn btn_main btn_round" href="#">
            Contact Us
            <svg
              className="ms-2"
              xmlns="http://www.w3.org/2000/svg"
              width={18}
              height={18}
              viewBox="0 0 24 25"
              fill="none"
            >
              <path
                d="M5.49002 0.47998V2.91198H19.937L0.900024 23.0854L2.518 24.8L21.555 4.62654V19.936H23.85V0.47998H5.49002Z"
                fill="white"
              />
            </svg>
          </a>
        </div>
      </section>
    </Layout>
  );
};

export default CloudSolutions;
