import Layout from "../components/Layout";

const MobileDevelopment = () => {
  return (
    <Layout page="mobile-development">
      <section>
        <div className="title-container">
          <div className="title">
            <div className="breadcrums mt-2">
              <a href="#">Home</a>
              &gt;
              <a href="#">Services</a>
              &gt; Mobile Development
            </div>
            <p>Design</p>
            <h2>
              Mobile
              <br />
              Development
            </h2>
            <a className="btn btn_main btn_round" href="#">
              Book Now
            </a>
          </div>
          <img
            className="title-img pe-5"
            src="./assets/mobile-development/Mobile-Development-1.png"
            alt="title-img"
          />
        </div>
        <div className="section-1 d-flex align-items-center">
          <img className="mt-5" src="./assets/Dots.png" alt="dots" />
          <img
            src="./assets/mobile-development/Mobile-Development-2.gif"
            alt="section-1"
          />
          <div className="d-flex flex-column">
            <span>
              We design and create mobile applications with cutting-edge
              technologies, while focusing on our users’ experience.
            </span>
            <span>
              By prioritising usability, we produce mobile apps that are
              innovative, informative and easy to use.
            </span>
          </div>
        </div>
        <div className="section-2 container ps-4">
          <h2>Our Process</h2>
          <div className="card-container">
            <div className="card">
              <img
                className="mt-3 me-5 pe-5"
                src="./assets/mobile-development/01.png"
                alt="number-01"
              />
              <p>Understanding Your Business Needs</p>
              <img
                src="./assets/mobile-development/Group-1.png"
                alt="group-1"
              />
              <span className="px-1">
                Quality application demands creative &amp; professional inputs.
                By working and-in-hand together with you, it ensures excellent
                execution throughout each part of the development process.
              </span>
            </div>
            <div className="card">
              <img
                className="mt-3 me-5 pe-5"
                src="./assets/mobile-development/02.png"
                alt="number-02"
              />
              <p>Strategy Implementation</p>
              <img
                src="./assets/mobile-development/Group-2.svg"
                alt="group-2"
              />
              <span>
                Our team of experienced designers will turn your ideas into
                reality with wireframes, user experience (UX) mapping,
                transforming and creating graphical sections, branding assets,
                and style guide.
              </span>
            </div>
            <div className="card">
              <img
                className="mt-3 me-5 pe-5"
                src="./assets/mobile-development/03.png"
                alt="number-03"
              />
              <p>Creativity And Ideation</p>
              <img
                src="./assets/mobile-development/Group-3.svg"
                alt="group-3"
              />
              <span>
                Developers step in to bring your ideas to life. Developing
                databases, core logic, and dynamic screen behaviours ensures a
                rapid, seamless and interactive experience for your users.
              </span>
            </div>
            <div className="card">
              <img
                className="mt-3 me-5 pe-5"
                src="./assets/mobile-development/04.png"
                alt="number-04"
              />
              <p className="px-2">The Great Launch</p>
              <img
                src="./assets/mobile-development/Group-4.svg"
                alt="group-4"
              />
              <span>
                Thorough and meticulous testing on all iPad, Android, and iPhone
                app throughout the development process to ensure a seamless
                mobile app experience. A series of QA test on app performance
                and functionality takes place before submission.
              </span>
            </div>
          </div>
        </div>
        <div className="stay_connect d-flex justify-content-between">
          <div className="ps-2">
            <h2>
              Stay Connected
              <br />
              Get to know us, Say hello!
            </h2>
            <ul>
              <li>Mobile Development</li>
              <li>Website Development</li>
              <li>Enterprise Solutions</li>
              <li>Cloud Solutions</li>
              <li>UI/UX Design</li>
              <li>QA Testing</li>
              <li>Digital Marketing</li>
              <li>IT Consulting</li>
            </ul>
            <a className="btn btn_main btn_round" href="#">
              Contact Us
            </a>
          </div>
          <div>
            <img
              width={600}
              height={500}
              src="./assets/Stay-Connected.png"
              alt="stay-connect"
            />
            <img className="pt-5" src="./assets/Dots.png" alt="dots" />
          </div>
        </div>
        <div className="contact">
          <h2>
            Ready to turn
            <br />
            your ideas into reality?
          </h2>
          <p>
            Contact us now to know more on how you can leverage your business!
          </p>
          <a className="btn btn_main btn_round" href="#">
            Contact Us
            <svg
              className="ms-2"
              xmlns="http://www.w3.org/2000/svg"
              width={18}
              height={18}
              viewBox="0 0 24 25"
              fill="none"
            >
              <path
                d="M5.49002 0.47998V2.91198H19.937L0.900024 23.0854L2.518 24.8L21.555 4.62654V19.936H23.85V0.47998H5.49002Z"
                fill="white"
              />
            </svg>
          </a>
        </div>
      </section>
    </Layout>
  );
};

export default MobileDevelopment;
